
import json
from datetime import datetime
from urllib.parse import urlparse

from django.conf import settings

from .utils import get_ooyala_api, get_exception, to_str, clean_force


class OoyalaClient(object):
    def __init__(self):
        super(OoyalaClient, self).__init__()
        self.OOYALA_MAIN_LABEL_ID = getattr(
            settings, 'OOYALA_MAIN_LABEL_ID', ''
        )
        self.ooyala_api = get_ooyala_api()

    def create_assets(self, size, samba_instance, **kwargs):
        descripcion = samba_instance.data.get('description', '') or \
                samba_instance.data.get('shortDescription', '') or ''

        if isinstance(descripcion, (tuple, list)):
            descripcion = "".join([str(_) for _ in descripcion])

        file_name = self.get_file_name(kwargs.get('file_name', 'prueba'))
        response_post, status_post = self.ooyala_api.post(
            'v2/assets', {
                "name": file_name.split(".")[0],
                "file_name": file_name,
                "asset_type": kwargs.get('qualifier', 'video'),
                "file_size": size,
                "download_allowed": 'Y',
                "description": to_str(clean_force(descripcion)),
                "external_id": kwargs.get('external_id', '')
            }
        )
        if status_post//100 == 2:
            response_post = json.loads(to_str(response_post))
            samba_instance.status_ooyala = 'PENDING'
            samba_instance.embed_code = response_post['embed_code']
            samba_instance.save()
            return response_post['embed_code']

        elif status_post//100 == 4:
            return False

        raise get_exception(
            "ERROR_5", status_post, 0, response_post)

    def get_file_name(self, file_name):
        hash_time = str(datetime.now()).split(
            '.')[0].replace('-', '').replace(':', '').replace(' ', '_')
        name_split = file_name.split(".")
        return name_split[0] + '_' + hash_time + '.' + name_split[1]

    def get_url_up(self, embed):
        response_url, status_url = self.ooyala_api.get(
            'v2/assets/{0}/uploading_urls'.format(embed)
        )

        if status_url//100 == 2:
            return json.loads(to_str(response_url))

        raise get_exception(
            "ERROR_7", status_url, 0, response_url)

    def upload_video_ooyala(self, url, embed, absolute_path, samba_instance):
        with open(absolute_path, 'rb') as buf:
            file_up = buf.read()

        url_upload = urlparse(url)
        path_upload = url_upload.path + "?" + url_upload.query
        other_base_url = to_str(url_upload.netloc)

        if path_upload[0] == "/":
            path_upload = path_upload[1:]

        response_put, status_put = self.ooyala_api.put(
            path_upload,
            file_up,
            new_base_url=other_base_url
        )

        if status_put//100 == 2:
            samba_instance.status_ooyala = 'UPLOADING'
            samba_instance.save()
            return True

        raise get_exception(
            "ERROR_4", status_put, 0, to_str(response_put))

    def changed_status(self, status, embed, samba_instance):
        response_status, estado_status = self.ooyala_api.put(
            'v2/assets/{0}/upload_status'.format(embed),
            {"status": status}
        )

        if estado_status // 100 == 2:
            response_status = json.loads(to_str(response_status))
            estado_final = response_status.get('status', '')

            if estado_final == status:
                samba_instance.status_ooyala = 'PROCCESSING'
                samba_instance.save()
                return True

            raise get_exception(
                "ERROR_2", estado_final, 0, response_status)

        raise get_exception(
            "ERROR_3", estado_status, 0, response_status)

    def asociated_label(self, embed, tag_label, year=None):
        list_labels = [self.OOYALA_MAIN_LABEL_ID]
        response_labels = self.get_labels()
        dicc_names = self.parse_dicc_label(response_labels)

        if tag_label:
            if tag_label in dicc_names:
                list_labels.append(dicc_names[tag_label])
            elif tag_label != '_null':
                tag_create = self.create_label(tag_label)
                if tag_create and isinstance(tag_create, dict):
                    list_labels.append(tag_create["id"])

        if year:
            year = str(year)
            if year in dicc_names:
                list_labels.append(dicc_names[year])
            elif year != '_null':
                tag_create = self.create_label(year)
                if tag_create and isinstance(tag_create, dict):
                    list_labels.append(tag_create["id"])

        return self.send_labels(embed, list_labels)

    def parse_dicc_label(self, response_labels):
        dicc_names = {}
        if 'items' in response_labels and isinstance(response_labels, dict):
            dicc_names = {
                _["name"]: _['id'] for _ in response_labels["items"]
            }
        return dicc_names

    def del_labels(self, list_del):
        response_labels = self.get_labels()
        dicc_names = self.parse_dicc_label(response_labels)

        for label in list_del:
            if label in dicc_names:
                id_label = dicc_names[label]
                self.del_label(id_label)

    def del_label(self, id_label):
        rsp, st = self.ooyala_api.delete(
            'v2/labels/{0}'.format(
                id_label
            ))

    def get_labels(self):
        response_labels, status_labels = self.ooyala_api.get(
            'v2/labels/{}/children'.format(self.OOYALA_MAIN_LABEL_ID)
        )
        if status_labels // 100 == 2:
            return json.loads(response_labels)
        return {}

    def create_label(self, nodo_label):
        rsp, st = self.ooyala_api.post(
            'v2/labels', {
                "name": nodo_label,
                "parent_id": self.OOYALA_MAIN_LABEL_ID
            })
        if st // 100 == 2:
            return json.loads(to_str(rsp))

        return False

    def send_labels(self, embed, list_labels):
        response_ll, status_ll = self.ooyala_api.post(
            'v2/assets/{0}/labels'.format(embed), list_labels
        )
        if status_ll // 100 == 2:
            return list_labels
        return []

    def get_assets(self, embed):
        r_get, r_status = self.ooyala_api.get(
            'v2/assets/{0}'.format(embed))

        return r_get, r_status

    def clean_labels(self, embed):
        image_get, image_status = self.ooyala_api.delete(
            "v2/assets/{0}/labels".format(embed),
        )
        return image_status // 100 == 2

    def up_image_preview(self, embed, samba_instance):
        path_img = samba_instance.samba_img.path
        with open(path_img, 'rb') as fi:
            image_up = fi.read()

        image_post, image_status = self.ooyala_api.post(
            "v2/assets/{0}/preview_image_files".format(embed),
            image_up,
            up_file=True
        )

        if image_status // 100 == 2:
            samba_instance.status_img = True
            samba_instance.save()
            return True

        return False

    def clean_images_preview(self, embed):
        image_get, image_status = self.ooyala_api.delete(
            "v2/assets/{0}/preview_image_files".format(embed),
        )
        return image_status // 100 == 2

    def del_asset(self, embed):
        response_del, status_del = self.ooyala_api.delete(
            'v2/assets/{0}'.format(embed)
        )
        return status_del // 100 == 2
