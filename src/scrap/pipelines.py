import os

from scrapy.exceptions import DropItem
from scrapy.pipelines.files import FilesPipeline
from scrapy.pipelines.images import ImagesPipeline
import scrapy


class DownloadPipeline(FilesPipeline):

    def get_media_requests(self, item, info):
        request = scrapy.Request(item['url'])
        request.meta['instance'] = item.instance
        return request

    def file_path(self, request, response=None, info=None):
        instance = request.meta['instance']
        media_ext = os.path.splitext(request.url)[1]

        file_path = '{qualifier}/{year}/{month}/{samba_id}{media_ext}'.format(
            qualifier=instance.qualifier,
            year=instance.postdate.year,
            month=str(instance.postdate.month).zfill(2),
            samba_id=instance.samba_id,
            media_ext=media_ext
        )
        return file_path

    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]

        if not image_paths:
            raise DropItem("No se descargó ningún archivo")
        else:
            item['file_path'] = image_paths[0]

        return item


class MyImagesPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        if item['image_url'].strip():
            request = scrapy.Request(item['image_url'])
            request.meta['instance'] = item.instance
            return request

    def file_path(self, request, response=None, info=None):
        instance = request.meta['instance']
        media_ext = os.path.splitext(request.url)[1]

        file_path = 'thumbs/{year}/{month}/{samba_id}{media_ext}'.format(
            year=instance.postdate.year,
            month=str(instance.postdate.month).zfill(2),
            samba_id=instance.samba_id,
            media_ext=media_ext
        )
        return file_path

    def item_completed(self, results, item, info):
        image_paths = [x['path'] for ok, x in results if ok]

        if image_paths:
            item['img_path'] = image_paths[0]

        return item


class OoyalaPipeline(object):

    def process_item(self, item, spider):
        instance = item.instance

        instance.samba_file = item['file_path']
        if instance.qualifier == 'video':
            instance.samba_img = item['img_path']

        instance.push_to_ooyala()
        instance.save()

        return item
