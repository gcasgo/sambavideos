import os

from django.conf import settings
from django.core.management.base import BaseCommand
import requests

from ...models import SambaFile


class Command(BaseCommand):
    help = 'prints no args if there are no args'

    def handle(self, *args, **options):

        for item in SambaFile.objects.filter(status_ooyala='NOT FOUND'):
            print(item.id, item.url)
            item.samba_file = self.download_file(item.url, item.qualifier)
            item.samba_img = self.download_file(item.url_img, 'thumbs')
            item.save()

    def download_file(self, url, mkdir='video'):
        samba_file = mkdir + '/' + url.split('/')[-1]
        local_filename = os.path.join(settings.MEDIA_ROOT, samba_file)
        # NOTE the stream=True parameter
        r = requests.get(url, stream=True)
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    f.flush()
        return samba_file
