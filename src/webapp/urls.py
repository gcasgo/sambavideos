from django.urls import path

from .views import ooyalavideo

urlpatterns = [
    path(
        'assets/<str:slug>/', ooyalavideo, name='ooyalavideo'
    ),
]
