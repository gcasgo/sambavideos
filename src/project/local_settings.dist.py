ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'PROJECT',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': '127.0.0.1',
    }
}

SAMBA_ACCESS_TOKEN = 'SAMBA_ACCESS_TOKEN'
SAMBA_APP_ID = 'SAMBA_APP_ID'

SCRAPY_LOG_FILE = ''
SCRAPY_LOG_LEVEL = 'DEBUG'

# OOYALA
OOYALA_API_KEY = 'OOYALA_API_KEY'
OOYALA_SECRET_KEY = 'OOYALA_SECRET_KEY'
OOYALA_MAIN_LABEL_ID = u'bfe0dc207b014dabbe2bd4dbceca1f22'
# OOYALA_MAIN_LABEL_ID = u'c67fff58abdd46dba70a05a9facd5c89'  # Producción
