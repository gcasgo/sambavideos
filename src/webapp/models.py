import json
from datetime import datetime

from django.db import models
from ooyala.utils import clean_word
from ooyala.client import OoyalaClient


class SambaFile(models.Model):
    created = models.DateTimeField(
        null=True,
        auto_now_add=True
    )
    samba_id = models.CharField(
        max_length=100,
        unique=True,
    )
    status_ooyala = models.CharField(
        max_length=20,
        null=True,
        blank=True
    )
    samba_data = models.TextField(
        null=True,
        blank=True
    )
    ooyala_data = models.TextField(
        null=True,
        blank=True
    )
    last_ooyala_data = models.TextField(
        null=True,
        blank=True
    )
    origin_page = models.IntegerField(
        null=True,
        blank=True
    )
    url = models.URLField(
        null=True,
        blank=True,
        max_length=500
    )
    duration = models.IntegerField(
        null=True,
        blank=True
    )
    size = models.IntegerField(
        null=True,
        blank=True
    )
    qualifier = models.CharField(
        max_length=20,
        null=True,
        blank=True
    )
    samba_file = models.FileField(
        verbose_name=u'Archivo',
        upload_to="samba_file",
        null=True,
        blank=True,
        max_length=500
    )
    embed_code = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )
    published = models.DateTimeField(
        null=True,
        blank=True
    )
    postdate = models.DateTimeField(
        null=True,
        blank=True
    )
    label = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )
    ooyala_label_id = models.CharField(
        max_length=200,
        null=True,
        blank=True
    )
    url_img = models.URLField(
        null=True,
        blank=True,
        max_length=500
    )
    samba_img = models.FileField(
        verbose_name=u'Imagen previa',
        upload_to="samba_img",
        null=True,
        blank=True,
        max_length=500
    )
    status_img = models.BooleanField(
        default=False
    )
    check = models.BooleanField(
        default=False
    )

    def __str__(self):
        return '{0} - {1}'.format(self.embed_code, str(self.created))

    @property
    def data(self):
        if not hasattr(self, '_data'):
            self._data = json.loads(self.samba_data)
        return self._data

    def load_data(self):
        files = self.data.get('files', [])
        published = self.data.get('publishDate', '0')
        label = self.data.get('categoryName', '')
        postdate = self.data.get('postdate', '0')
        thumbs = self.data.get('thumbs', [])
        duration, url, size = 0, '', 0

        if published:
            published = datetime.fromtimestamp(published / 1e3)

        if postdate:
            postdate = datetime.fromtimestamp(postdate / 1e3)

        if label:
            label = clean_word(label)

        main_file = None
        if len(files) == 1:
            main_file = files[0]
        elif len(files) > 1:
            main_file = files[0]
            for file in files[1:]:
                file_size = file.get('fileSize', 0)
                if file['outputId'] != 0 and \
                        file_size > main_file.get('fileSize', 0):
                    main_file = file

        if main_file:
            size = int(main_file.get('fileSize', 0))
            url = main_file.get('url', '')
            duration = main_file['fileInfo'].get('duration', 0)

        main_img = None
        if len(thumbs) == 1:
            main_img = thumbs[0]
        elif len(thumbs) > 1:
            main_img = thumbs[0]
            for img in thumbs[1:]:
                if img.get('size', 0) > main_img.get('size', 0):
                    main_img = img

        url_img = ''
        if main_img:
            url_img = main_img.get('url', '')

        self.qualifier = self.data['qualifier'].lower()

        if not self.url_img:
            self.url_img = url_img
        if not self.url:
            self.url = url
        if not self.duration:
            self.duration = duration if duration else None
        if not self.size:
            self.size = size
        if not self.published and published:
            self.published = published
        if not self.postdate and postdate:
            self.postdate = postdate
        if not self.label and label and label != '_null':
            self.label = label

    def save(self, *args, **kwargs):

        if not self.embed_code:
            self.embed_code = ''

        if not self.samba_file:
            self.samba_file = ''

        super(SambaFile, self).save(*args, **kwargs)

    def push_to_ooyala(self):
        ooyala_client = OoyalaClient()
        absolute_path = self.samba_file.path
        size = self.samba_file.size
        file_name = self.url.split('/')[-1]

        if str(self.qualifier).upper() not in ['VIDEO', 'AUDIO']:
            return self.save()

        if self.status_ooyala not in ['PROCCESSING', 'LIVE']:

            if self.status_ooyala == 'PENDING':
                embed_code = self.embed_code
            else:
                embed_code = ooyala_client.create_assets(
                    size,
                    self,
                    file_name=file_name,
                    external_id=self.samba_id,
                    qualifier=self.qualifier
                )
            print("embed_code :: ", embed_code)
            if embed_code:
                if self.status_ooyala != 'UPLOADING':
                    print("up")
                    url_up_video = ooyala_client.get_url_up(embed_code)
                    ooyala_client.upload_video_ooyala(
                        url_up_video[0],
                        embed_code,
                        absolute_path,
                        self
                    )

                ooyala_client.changed_status(
                    'uploaded',
                    embed_code,
                    self
                )

                ooyala_data, status = ooyala_client.get_assets(embed_code)
                self.ooyala_data = str(ooyala_data)
                self.save()

        if self.embed_code and \
                not self.ooyala_label_id:
            list_label = ooyala_client.asociated_label(
                self.embed_code, self.label, self.postdate.year
            )
            self.ooyala_label_id = ", ".join(list_label)
            self.save()

        if self.qualifier.lower() == 'video' \
                and self.embed_code and self.samba_img and not self.status_img:

            clean_preview = ooyala_client.clean_images_preview(
                self.embed_code
            )
            if clean_preview:
                ooyala_client.up_image_preview(
                    self.embed_code,
                    self
                )
