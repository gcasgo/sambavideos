from django import forms
from .witgets import JsonWidget

from .models import SambaFile


class SambaFileAdminForm(forms.ModelForm):

    class Meta:
        model = SambaFile
        widgets = {
          'samba_data': JsonWidget,
          'ooyala_data': JsonWidget
        }
        fields = '__all__'
