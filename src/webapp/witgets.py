import json
from pprint import pformat
from django import forms
from django.utils.safestring import mark_safe


class JsonWidget(forms.Widget):

    def render(self, name, value, attrs=None):
        read_field = json.loads(value) if value else ''
        data = pformat(read_field)
        html = '<textarea disabled style="float:left;width:80%;" rows="20">{echo}</textarea>' \
            '<input type="hidden" name="{name}" value="{value}">'

        return mark_safe(
            html.format(echo=data, value=str(value), name=name)
        )
