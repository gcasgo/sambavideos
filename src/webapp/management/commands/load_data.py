from django.core.management.base import BaseCommand

from webapp.models import SambaFile


class Command(BaseCommand):
    help = "Create Label por integrations Samba - Ooyala."

    def handle(self, *args, **options):
        for item in SambaFile.objects.all():
            item.load_data()
            item.save()
