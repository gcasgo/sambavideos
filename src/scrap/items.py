# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy_djangoitem import DjangoItem

from webapp.models import SambaFile


class ScrapItem(DjangoItem):
    django_model = SambaFile
    samba_id = scrapy.Field()
    url = scrapy.Field()
    image_url = scrapy.Field()
    qualifier = scrapy.Field()
    postdate = scrapy.Field()
    file_path = scrapy.Field()
    img_path = scrapy.Field()
    tag = scrapy.Field()

    def set_instance(self, instance):
        self._instance = instance
