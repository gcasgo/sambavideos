from django.core.management.base import BaseCommand

from ...models import SambaFile


class Command(BaseCommand):
    help = 'Compara el tamaño de los archivos descargados con los registrados en SAMBA'

    def handle(self, *args, **options):
        for samba_instance in SambaFile.objects.filter(size__gt=0):
            if samba_instance.samba_file and samba_instance.size:
                if samba_instance.size != samba_instance.samba_file.size:
                    print(
                        'ERROR',
                        samba_instance.size,
                        samba_instance.samba_file.size,
                        samba_instance.size/1000/1000,
                        samba_instance.samba_id
                    )
