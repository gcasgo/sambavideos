from django.core.management.base import BaseCommand
from django.conf import settings
from django_bulk_update.helper import bulk_update
from django.db import transaction

from ooyala.utils import get_ooyala_api
from webapp.models import SambaFile


parent_id = getattr(settings, 'OOYALA_MAIN_LABEL_ID', '')
print("parent_id :: ", parent_id)


class Command(BaseCommand):
    help = "Create Label por integrations Samba - Ooyala."
    ooyala_api = get_ooyala_api()

    def handle(self, *args, **options):
        model_samba = SambaFile.objects.filter(
            status_ooyala__in=['PROCCESSING', 'PENDING', 'UPLOADING', 'LIVE'],
            embed_code__isnull=False,
        )

        for instance in model_samba:
            rsp, st = self.ooyala_api.delete(
                'v2/assets/{0}'.format(instance.samba_id))
            if not st//100 == 2:
                print(rsp)
            print("/////////////////////////////////////")

            instance.embed_code = ''
            instance.status_ooyala = ''
            instance.ooyala_label_id = ''
            instance.status_img = False

        with transaction.atomic():
            bulk_update(
                model_samba, update_fields=[
                    'status_ooyala', 'ooyala_label_id',
                    'embed_code', 'status_img']
            )
