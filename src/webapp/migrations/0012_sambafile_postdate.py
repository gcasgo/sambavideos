# Generated by Django 2.0.1 on 2018-01-25 17:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0011_auto_20180125_1506'),
    ]

    operations = [
        migrations.AddField(
            model_name='sambafile',
            name='postdate',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
