from django.contrib import admin
from django.conf import settings
from django.utils.safestring import mark_safe

from .models import SambaFile
from .forms import SambaFileAdminForm


@admin.register(SambaFile)
class SambaFileAdmin(admin.ModelAdmin):
    form = SambaFileAdminForm
    readonly_fields = ('samba_id', )
    list_display = ['postdate', 'qualifier', 'get_links', 'duration', 'size',
                    'status_ooyala', 'check']
    # list_editable = ('check',)
    list_filter = ['status_ooyala', 'qualifier', 'check', 'ooyala_label_id']
    search_fields = ('samba_id', 'url', 'ooyala_label_id')
    list_per_page = 100

    actions = ['push_to_ooyala']

    def push_to_ooyala(self, request, queryset):
        for item in queryset:
            if item.samba_file:
                item.push_to_ooyala()
            else:
                self.message_user(request, "Item ID %s no tiene archivo." % item.id)

    push_to_ooyala.short_description = "Subir a Ooyala"

    def get_link_ooyala_amp(self, ooyala_code):
        return 'https://player.ooyala.com/iframe.html?ec={0}&pbid={1}&platform=html5'.format(
            ooyala_code,
            settings.OOYALA_PLAYER_ID
        )

    def get_links(self, obj):
        list_null = ['javascript:void(0)', 'text-decoration:none; color:red']
        list_val = [obj.url, ''] if obj.url else list_null
        if obj.embed_code:
            list_val.extend([self.get_link_ooyala_amp(obj.embed_code), ''])
        else:
            list_val.extend(list_null)
        html = '<a href="{0}" style="{1}">Samba</a> | <a href="{2}" style="{3}">Ooyala</a>'.format(*list_val)
        return mark_safe(html)

    get_links.short_description = u'Enlaces'
