import json

from django.conf import settings
from django.core.management.base import BaseCommand

from ooyala.client import OoyalaClient
from webapp.models import SambaFile


parent_id = getattr(settings, 'OOYALA_MAIN_LABEL_ID', '')
print("parent_id :: ", parent_id)


ooyala_client = OoyalaClient()


class Command(BaseCommand):
    help = "Revisa los videos subidos y actualiza los que tengan estado LIVE"
    ooyala_api = ooyala_client.ooyala_api

    def add_arguments(self, parser):
        parser.add_argument('year', nargs='?', type=int, default=0)

    def handle(self, *args, **options):
        year = options.get('year', None)

        queryset = SambaFile.objects.exclude(status_ooyala__in=['LIVE', 'SAMBA ERROR'])
        if year:
            print("> > YEAR: ", year)
            queryset = queryset.filter(postdate__year=year)
        else:
            print('TODOS check=no')

        for instance in queryset:

            rsp, http_code = self.ooyala_api.get(
                'v2/assets/{0}'.format(instance.samba_id)
            )

            if not instance.samba_id:
                print('SIN SAMBA ID', instance.id)
                continue

            if http_code // 100 == 2:
                instance.check = True
                instance.last_ooyala_data = rsp

                rsp = json.loads(rsp)
                status = rsp.get('status', '').upper()

                instance.status_ooyala = status
                if status == 'LIVE':
                    instance.embed_code = rsp.get('embed_code', '')

                elif status == 'ERROR':
                    print(rsp)
                    instance.ooyala_label_id = ''
                    instance.status_img = False
                    instance.embed_code = ''
                    ooyala_client.del_asset(instance.samba_id)

                if rsp['external_id'] != instance.samba_id:
                    print("EXTERNAL ID ERROR {}".format(instance.id))

                if instance.embed_code and rsp['embed_code'] != instance.embed_code:
                    print("EMBED CODE ERROR {}".format(instance.id))

            else:
                if instance.qualifier == 'image':
                    instance.check = True

                instance.status_ooyala = 'NOT FOUND'
                instance.ooyala_label_id = ''
                instance.status_img = False
                instance.embed_code = ''

                # if instance.embed_code:
                #     print("NO SE ENCONTRÓ CON SAMBA ID {}".format(instance.id))

                #     rsp, http_code = self.ooyala_api.get(
                #         'v2/assets/{0}'.format(instance.embed_code)
                #     )
                #     if http_code // 100 == 2:
                #         instance.last_ooyala_data = rsp

                #         rsp = json.loads(rsp)
                #         if rsp['external_id'] != instance.samba_id:
                #             print("ID ERROR {}".format(instance.id))

                #         if instance.embed_code and rsp['embed_code'] != instance.embed_code:
                #             print("EMBED CODE ERROR {}".format(instance.id))

            if instance.data.get('childStatus', '') == 'ERROR':
                instance.check = True
                instance.status_ooyala = 'SAMBA ERROR'
                instance.ooyala_label_id = ''
                instance.status_img = False
                instance.embed_code = ''

            instance.save()

        count = SambaFile.objects.filter(status_ooyala='LIVE').count()
        print("> {} completos".format(count))

        count = SambaFile.objects.exclude(status_ooyala__in=['LIVE', 'ERROR', 'SAMBA ERROR']).count()
        print("> {} pendientes".format(count))
