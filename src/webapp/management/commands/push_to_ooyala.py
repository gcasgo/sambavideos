from django.core.management.base import BaseCommand
from django.conf import settings

from ooyala.client import OoyalaClient
from webapp.models import SambaFile


parent_id = getattr(settings, 'OOYALA_MAIN_LABEL_ID', '')
print("parent_id :: ", parent_id)


class Command(BaseCommand):
    help = "Create Label por integrations Samba - Ooyala."
    ooyala_api = OoyalaClient()

    def add_arguments(self, parser):
        parser.add_argument('year', nargs='?', type=int, default=0)

    def handle(self, *args, **options):
        year = options.get('year', None)
        queryset = SambaFile.objects.filter(status_ooyala='NOT FOUND').exclude(qualifier='image')
        if year:
            print("year : ", year)
            queryset = queryset.filter(postdate__year=year).order('?')

        print(queryset.count(), "pendientes")
        for instance in queryset:
            if instance.samba_file:
                instance.push_to_ooyala()
            else:
                print("SIN FILE")
