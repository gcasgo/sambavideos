from unicodedata import normalize, category

from django.conf import settings
from .api import OoyalaAPI

API_KEY = getattr(settings, 'OOYALA_API_KEY', None)
SECRET_KEY = getattr(settings, 'OOYALA_SECRET_KEY', None)

dicc_error = {
    'ERROR_1': 'falla en carga de vista previa, ',
    'ERROR_2': 'status no marcado como cargado, ',
    'ERROR_3': 'actualizacion de estado de video, ',
    'ERROR_4': 'carga de video a Ooyala, ',
    'ERROR_5': 'creacion de activo Ooyala, ',
    'ERROR_6': 'eliminacion de imagnes previas, ',
    'ERROR_7': 'Obtencion de url de carga, '
}


def get_ooyala_api():
    if API_KEY and SECRET_KEY:
        return OoyalaAPI(API_KEY, SECRET_KEY)
    return False


def getSize(fileobject):
    fileobject.seek(0, 2)
    size = fileobject.tell()
    return size


def get_exception(tipo_error, status, idd, detalle):
    return Exception(
        dicc_error[tipo_error] + "status[{0}], Media id[{1}], Detalle{2}".format(
            status, idd, detalle)
    )


def to_str(bytes_or_str):
    if isinstance(bytes_or_str, bytes):
        return bytes_or_str.decode('utf-8')
    return bytes_or_str


def clean_word(word):
    return ''.join((_ for _ in normalize('NFD', word) if category(_) != 'Mn'))


def clean_force(word):
    return ''.join(
        (_ for _ in normalize('NFD', word)
         if category(_) in ['Ll', 'Lu', 'Zs', 'Nd'])
    )


def parse_fecha_ooyala(fecha_str):
    list_fecha = \
        fecha_str.replace('T', '-').replace(':', '-').replace('Z', '').split('-') \
        if fecha_str else [1980, 1, 1]

    return [int(num) for num in list_fecha]
