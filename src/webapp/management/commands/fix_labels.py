from django.conf import settings
from django.core.management.base import BaseCommand

from ooyala.client import OoyalaClient
from webapp.models import SambaFile


class Command(BaseCommand):
    help = 'prints no args if there are no args'

    def handle(self, *args, **options):

        ooyala_client = OoyalaClient()
        labels = [l['id'] for l in ooyala_client.get_labels()["items"]]
        labels.append(settings.OOYALA_MAIN_LABEL_ID)

        sambafile_list = []

        for sambafile in SambaFile.objects.all():
            if sambafile.ooyala_label_id:
                for label_id in sambafile.ooyala_label_id.split():
                    label_id = label_id.replace(',', '').replace(' ', '')
                    if label_id not in labels:
                        sambafile_list.append(sambafile)
            else:
                sambafile_list.append(sambafile)

        for sambafile in sambafile_list:
            if sambafile.qualifier != 'image' and sambafile.status_ooyala == 'LIVE':
                print("SAMBA ID: ", sambafile.samba_id)
                print(sambafile.ooyala_label_id)

                ooyala_client.clean_labels(sambafile.embed_code)
                list_label = ooyala_client.asociated_label(
                    sambafile.embed_code, sambafile.label, sambafile.postdate.year
                )
                sambafile.ooyala_label_id = ", ".join(list_label)
                sambafile.save()

                print(sambafile.ooyala_label_id)
                print('--')
