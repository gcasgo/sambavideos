from scrap.items import ScrapItem
from scrapy.spiders import CrawlSpider
from webapp.models import SambaFile


class SpiderBase(CrawlSpider):
    name = 'sambavideos'
    parser_class = None
    start_urls = ['https://sambavideos.docs.apiary.io', ]

    def __init__(self, year=None, **kwargs):
        self.year = int(year) if year else None
        super().__init__(**kwargs)

    def parse_start_url(self, response):

        items = SambaFile.objects.exclude(status_ooyala__in=['LIVE', 'SAMBA ERROR'])

        if self.year:
            self.log('AÑO: %s' % self.year)
            items = items.filter(postdate__year=self.year)

        self.log(u"// BASE DE DATOS %s URLS" % items.count())
        for instance_id in items.values_list('id', flat=True):

            instance = SambaFile.objects.get(id=instance_id)
            instance.load_data()
            item = ScrapItem()
            item['url'] = instance.url
            item['image_url'] = instance.url_img
            item['samba_id'] = instance.samba_id
            item['qualifier'] = instance.qualifier or 'video'
            item['postdate'] = instance.postdate
            item['tag'] = instance.label
            item.set_instance(instance)
            yield item
