from django.core.management.base import BaseCommand
from django.conf import settings

from ooyala.client import OoyalaClient
from webapp.models import SambaFile


parent_id = getattr(settings, 'OOYALA_MAIN_LABEL_ID', '')
print("parent_id :: ", parent_id)


class Command(BaseCommand):
    help = "Create Label por integrations Samba - Ooyala."
    ooyala_api = OoyalaClient()

    def handle(self, *args, **options):
        model_samba = SambaFile.objects.filter(
            status_ooyala__in=['PROCCESSING', 'PENDING', 'UPLOADING', 'LIVE'],
            embed_code__isnull=False,
        )

        for instance in model_samba:
            ooyala_data, status = \
                self.ooyala_api.get_assets(instance.embed_code)
            print(ooyala_data)
            print(status)
            print("///////////////////////")
