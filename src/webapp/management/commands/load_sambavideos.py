import json
from django.core.management.base import BaseCommand
import requests

from ...models import SambaFile


class Command(BaseCommand):
    help = 'prints no args if there are no args'

    def add_arguments(self, parser):
        parser.add_argument('start', nargs='?', type=int, default=0)

    def handle(self, *args, **options):
        start = options.get('start', None)

        url = 'http://api.sambavideos.sambatech.com/v1/medias'
        payload = {
            'access_token': 'cdcb92f7-ff6f-4e93-b218-7886e8c9939e',
            'pid': '1787',  # Project ID
            'limit': 100,
            'sort': 'ASC',
            'start': 1
        }

        for index in range(start, 1000000):
            print('\n> Página', index)
            file_list = []
            count_exists = 0
            count_news = 0
            payload['start'] = index
            response = requests.get(url, params=payload)
            result = response.json()
            print('= Resultado', len(result))
            for item in result:
                try:
                    samba_file = SambaFile.objects.get(samba_id=item['id'])
                except SambaFile.DoesNotExist:
                    file_list.append(
                        SambaFile(
                            samba_id=item['id'],
                            samba_data=json.dumps(item),
                            origin_page=index
                        )
                    )
                    count_news += 1
                else:
                    count_exists += 1
                    samba_file.samba_data = json.dumps(item)
                    samba_file.origin_page = index
                    samba_file.save()

            if count_news + count_exists == 0:
                break

            SambaFile.objects.bulk_create(file_list)
            print('* Duplicados', count_exists)
            print('+ Nuevos', count_news)

        for item in SambaFile.objects.filter(qualifier__isnull=True):
            item.load_data()
            item.save()

        SambaFile.objects.filter(qualifier='image').update(status_ooyala='LIVE')
        SambaFile.objects.filter(size=0).update(status_ooyala='LIVE')

        print('TOTAL:', SambaFile.objects.all().count(), 'archivos')
