from django.conf import settings


def constants(request):
    return {
        'player_id': getattr(settings, 'OOYALA_PLAYER_ID', ''),
        'rule_id': getattr(settings, 'OOYALA_RULE_ID', '')
    }
