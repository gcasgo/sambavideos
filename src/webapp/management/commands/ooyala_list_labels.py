import json
from pprint import pprint
from django.core.management.base import BaseCommand
from django.conf import settings

from ooyala.utils import get_ooyala_api


parent_id = getattr(settings, 'OOYALA_MAIN_LABEL_ID', '')
print("parent_id :: ", parent_id)


class Command(BaseCommand):
    help = "Lista los labels de ooyala"
    ooyala_api = get_ooyala_api()

    def handle(self, *args, **options):

        rsp, st = self.ooyala_api.get(
            'v2/labels'
        )
        if st == 200:
            pprint(json.loads(rsp)['items'])
        else:
            pprint(st)
