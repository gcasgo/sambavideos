import json

from django.core.management.base import BaseCommand
from django.conf import settings
from ooyala.utils import get_ooyala_api


parent_id = getattr(settings, 'OOYALA_MAIN_LABEL_ID', '')
print("parent_id :: ", parent_id)


class Command(BaseCommand):
    help = "Create Label por integrations Samba - Ooyala."
    ooyala_api = get_ooyala_api()

    def add_arguments(self, parser):
        parser.add_argument('label', nargs='?', type=str, default=0)

    def handle(self, *args, **options):
        label = options.get('label', None)

        print('Eliminando:', label)

        rsp, st = self.ooyala_api.get(
            'v2/labels'
        )
        if st == 200:
            for item in json.loads(rsp)['items']:

                if item['name'] == label or item['full_name'] == label:
                    rsp, st = self.ooyala_api.delete(
                        'v2/labels/{0}'.format(item['id'])
                    )
                    if not st//100 == 2:
                        print(rsp)
