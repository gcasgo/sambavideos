from django.core.management.base import BaseCommand
from django.db.models import Avg, Max, Min, Sum

from ...models import SambaFile


class Command(BaseCommand):
    help = 'prints no args if there are no args'

    def handle(self, *args, **options):
        print('>> TODOS')
        all_query = SambaFile.objects.filter(size__gt=0).exclude(qualifier='image')
        self.show(all_query)

        print('>> TODOS DESCARGADOS')
        query = all_query.exclude(samba_file='')
        self.show(query)

        print('>> TODOS OOYALA')
        query = all_query.filter(status_ooyala='LIVE')
        self.show(query)

        for year in range(2013, 2019):
            print('>> AÑO', year)
            year_query = all_query.filter(postdate__year=year)
            self.show(year_query)

            print('# DESCARGADOS')
            query = year_query.exclude(samba_file='')
            down_count = self.show(query)

            print('# SUBIDOS A OOYALA')
            query = year_query.filter(status_ooyala='LIVE')
            up_count = self.show(query)

            if down_count == up_count:
                print("> > ", year, " COMPLETO < <\n")
            else:
                diff = down_count - up_count
                print("--\n", diff, " Pendientes\n")

    def show(self, query):
        query = query.filter(size__gt=0)

        result = query.aggregate(
            Avg('size'), Max('size'), Min('size'), Sum('size')
        )
        if query.count():
            print("Tamaño mínimo:", result['size__min']/1e6, 'MB', query.filter(size=result['size__min'])[0].samba_file)
            print("Tamaño maximo:", result['size__max']/1e6, 'MB', query.filter(size=result['size__max'])[0].samba_file)
            print("Tamaño promedio:", result['size__avg']/1e6, 'MB')
            print("Tamaño total:", result['size__sum']/1e9, 'GB')
            print('-Videos-')
            result = query.filter(
                duration__isnull=False
            ).aggregate(
                Avg('duration'), Max('duration'), Min('duration'), Sum('duration')
            )
            print("Duración mínima:", result['duration__min']/1000, 's')
            print("Duración maxima:", result['duration__max']/1000, 's')
            print("Duración promedio:", result['duration__avg']/1000, 's')
            print("Duración total:", result['duration__sum']/1000, 's')
            print("--")

        count = query.count()
        print('Total:', count, "archivos\n")
        return count
