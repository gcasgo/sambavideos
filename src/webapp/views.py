from django.views.generic import DetailView
from django.views.decorators.csrf import csrf_exempt
from raven.contrib.django.raven_compat.models import client
from django.http import JsonResponse

from .models import SambaFile


class VideoOoyalaView(DetailView):
    model = SambaFile

    def get_queryset(self):
        slug = self.kwargs.get(self.slug_url_kwarg)

        samba_by = SambaFile.objects.get(ooyala_code=slug)

        return samba_by


@csrf_exempt
def ooyalavideo(request, slug):
    error, status = '', 200
    datus = {}
    try:
        if slug.__len__() < 25:
            error = 'Hash invalido'
            status = '400'
            datus['error'] = error
        else:
            origin = ''
            samba_file = SambaFile.objects.filter(embed_code=slug)

            if not samba_file.exists():
                samba_file = SambaFile.objects.filter(samba_id=slug)
            else:
                origin = 'embed_code'

            if not samba_file.exists():
                samba_file = SambaFile.objects.filter(samba_data__contains=slug)
            elif not origin:
                origin = 'samba_id'

            if samba_file.exists():
                samba_file = samba_file[0]
                datus.update({
                    'origin': origin if origin else 'samba_data',
                    'samba_id': samba_file.samba_id,
                    'embed_code': samba_file.embed_code,
                    'status_ooyala': samba_file.status_ooyala,
                    'postdate': samba_file.postdate.isoformat(),
                })
            else:
                error = 'Codigo no reconocido en Samba Data'
                status = '404'
                datus['error'] = error

    except Exception as e:
        error = str(e)
        datus['error'] = str(e)
        status = 500
        client.captureException()

    return JsonResponse(datus, status=status, reason=error)
