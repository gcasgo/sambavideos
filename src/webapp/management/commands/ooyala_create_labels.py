from django.core.management.base import BaseCommand
from django.conf import settings

from ooyala.utils import get_ooyala_api


parent_id = getattr(settings, 'OOYALA_MAIN_LABEL_ID', '')
print("parent_id :: ", parent_id)


class Command(BaseCommand):
    help = "Create Label por integrations Samba - Ooyala."
    ooyala_api = get_ooyala_api()

    def handle(self, *args, **options):
        # SOLO EJECUTAR UNA VEZ PARA EL LABEL PARENT

        rsp, st = self.ooyala_api.post(
            'v2/labels', {"name": "sambavideosprueba"}
        )
        print(rsp)
        print(st)
        """
        list_nodos = ['Diario El Bocon', 'Pruebas publicidad', 'Chica Correo',
                      'Cine', 'Diario Correo', 'Diario Ojo', 'DocuPeru',
                      'El Show', 'Gastronomía', 'Mujer Actual', 'Noticias',
                      'Teste']

        for nodo in list_nodos:
            rsp, st = self.ooyala_api.post(
                'v2/labels', {
                    "name": nodo,
                    "parent_id": parent_id
                }
            )
            print(rsp)
            print(st)
            print("==================================")
        """
        print("///////////////////////////////////////////////")
