import json

from django.core.management.base import BaseCommand
import requests

from ...models import SambaFile


class Command(BaseCommand):
    help = 'prints no args if there are no args'

    def add_arguments(self, parser):
        parser.add_argument('start', nargs='?', type=int, default=0)

    def handle(self, *args, **options):

        for item in SambaFile.objects.exclude(samba_stats=''):

            url = 'http://api.sambavideos.sambatech.com/v1/analytics/views/summarized/{}'.format(item.samba_id)
            payload = {
                'access_token': 'cdcb92f7-ff6f-4e93-b218-7886e8c9939e',
                'pid': '1787',  # Project ID
                'gmt': '-5',
                'channel': 'sambavideos',
                'period': 'last_month'
            }

            response = requests.get(url, params=payload)
            result = response.json()
            item.samba_stats = json.dumps(result)
            item.save()
